//
//  iOS_Code_ChallengeTests.swift
//  iOS Code ChallengeTests
//
//  Created by Jose González on 28/09/18.
//  Copyright © 2018 Jose González. All rights reserved.
//

import XCTest
@testable import iOS_Code_Challenge

class iOS_Code_ChallengeTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testExample() {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }
    
    func testPerformanceExample() {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    // TEST IOS CODE CHALLENGE
    
    func testAddTVShow()  {
        
        let VC = AddShowsViewController()
        let TVshow = "Friends"
        
        XCTAssertTrue(VC.saveTvShow(tvShow: TVshow))
    }
    
    
    
}
