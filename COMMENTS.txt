{\rtf1\ansi\ansicpg1252\cocoartf1561
{\fonttbl\f0\fswiss\fcharset0 Helvetica;}
{\colortbl;\red255\green255\blue255;}
{\*\expandedcolortbl;;}
\margl1440\margr1440\vieww10800\viewh8400\viewkind0
\pard\tx566\tx1133\tx1700\tx2267\tx2834\tx3401\tx3968\tx4535\tx5102\tx5669\tx6236\tx6803\pardirnatural\partightenfactor0

\f0\fs24 \cf0 Using Core data for data persistence, saving in two different entities for favorites and tvshows. \
You can delete the TV show  in the Favorites VC\
I added another View to make easy for the user to add new TV show. \
I follow all the instructions, And I haven't added too many new views, animations or alerts because when you are working whit clients, you have to follow only the established design patterns. \
I would like to use firebase to save information in a JSON tree, but I didn't have to much time, I have only work in this for 1 day. Because I have a lot of work for my actual job. \
I have made a simple test for addTVshow function, whit a bool return. }