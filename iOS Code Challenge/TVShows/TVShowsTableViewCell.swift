//
//  TVShowsTableViewCell.swift
//  iOS Code Challenge
//
//  Created by Jose González on 28/09/18.
//  Copyright © 2018 Jose González. All rights reserved.
//

import UIKit

class TVShowsTableViewCell: UITableViewCell {

    @IBOutlet weak var nameCell: UILabel!
    
    @IBOutlet weak var imageCell: UIImageView!
    
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
   
    
    
    
}
