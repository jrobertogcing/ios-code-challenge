//
//  DetailsViewController.swift
//  iOS Code Challenge
//
//  Created by Jose González on 29/09/18.
//  Copyright © 2018 Jose González. All rights reserved.
//

import UIKit

class DetailsViewController: UIViewController {

    
    @IBOutlet weak var nameLabel: UILabel!
    
    
    
    // variables
    var nameR = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        nameLabel.text = nameR

    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func moreInfoButtonAction(_ sender: UIButton) {
        
        let settingsUrl = NSURL(string : "https://www.imdb.com/title/tt0229888/?ref_=tt_rec_tt")! as URL
        UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
        
        
    }
    


}
